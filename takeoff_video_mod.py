import sys
import traceback
import tellopy
import av
import cv2.cv2 as cv2  # for avoidance of pylint error
import numpy
import time
from time import sleep
import threading

def videoCamera(drone):
    container = av.open(drone.get_video_stream())
    frame_skip = 300
    while True:
        i=0
        for frame in container.decode(video=0):
            i = i+1
            if i > frame_skip:
                if i%4==0: #do only 1/4 frames
                    im = numpy.array(frame.to_image())
                    im = cv2.resize(im, (320,240)) #resize frame
                    image = cv2.cvtColor(im, cv2.COLOR_RGB2BGR)
                    cv2.imshow('Original', image)
                    #cv2.imshow('Canny', cv2.Canny(image, 100, 200))
                    cv2.waitKey(1)

def ugoki(drone):
    drone.takeoff()
    print("takeoff")
    sleep(10)
    drone.down(50)
    print("down")
    sleep(5)
    drone.land()
    print("land")
    sleep(5)

def main():
    drone = tellopy.Tello()
    try:
        drone.connect()
        drone.wait_for_connection(60.0)
            
        
        video_th = threading.Thread(target=videoCamera,name="video_th",args=(drone))
        video_th.setDaemon(True)
        video_th.start()

        ugoki_th = threading.Thread(target=ugoki,name="ugoki_th",args=(drone))
        ugoki_th.setDaemon(True)
        ugoki_th.start()
    
        while True:
            in_ = sys.stdin.read(1)
            if in_ == "q":
                sleep(1)
                break

    except Exception as ex:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_traceback)
        print(ex)
    finally:
        drone.quit()
        cv2.destroyAllWindows()
        sys.exit()

if __name__ == '__main__':
    main()